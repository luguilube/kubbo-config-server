package com.formacionbdi.springboot.config.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class KubboConfigServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KubboConfigServerApplication.class, args);
	}

}
