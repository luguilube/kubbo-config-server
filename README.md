## Kubbo Test App

Kubbo Test App es una app basada en una idea inicial de la empresa Kubbo, específicamente como prueba técnica para postulación a una vacante. Esta app fue diseñada en un principio por un conjunto de 6 microservicios para el backend  (servidor, customers, products, warehouses, orders, cloud gateway) para gestionar específicamente lo referente a cada funcionalidad requerida por la App, pero segmentada en módulos; y para el frontend un proyecto únicamente. Actualmente se sigue actualizando que nuevas funcionalidades y tecnologías para ser mostrada como proyecto de portafolio.

## Descripción del Proyecto

Toda la descripción detallada del proyecto, se encuentra dentro del repositorio de [kubbo-test-app-frontend](https://bitbucket.org/luguilube/kubbo-test-app-frontend/src/master/) 

## Kubbo Config Server

Este microservicio es un servidor de configuraciones, leer mediante la dependencia Spring Config Server, archivos properties desde un repositorio GIT y todos los demás microservicios clientes, leen de él estas configuraciones y toman los valores correspondientes a las variables definidas en cada uno. Existen configuraciones a nivel general para todos los microservicios como la definición del servidor eureka mediante el cual se conectan todos menos este microservicio servidor de configuraciones; la configuración de la base de datos la cual es común para todos los microservicios, y también existen configuraciones específicas para ciertos microservicios como por ejemplo para el microservicio usuarios, donde este microservicio ejecuta un archivo import.sql con datos de usuarios de prueba y para poder ejecutarlo debe leer una configuración de la base que solo para este microservicio u otro que requiera ejecutar un import.sql.


## Nota

Se recomineda que este microservicio sea el primero en ser ejecutado, ya de que del dependen todos los demas microservicios exceptuando al microservicio servidor eureka: